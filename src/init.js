// Init your Fruit Vault (Identity Container)

const Index = require('./lib/lorena-index')

const main = async (didMethod, name, password) => {
  // Check needed vars to start
  new Index(didMethod)
    .new(name, password)
    .then(() => {
      console.log('Finished')
  })
}
main(process.argv[2], process.argv[3], process.argv[4])
