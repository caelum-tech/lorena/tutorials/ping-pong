const processAction = (system) => {
  return {

    // Updated Invitation.
    doStep: async (msg, _ctx) => {
      console.log('====== ACCEPTED INVITATION - SERVICE ======')
      msg.database.updateContact(msg.invitation.contact.did, msg.invitation.membership)
    }
  }
}

module.exports.processAction = processAction
