const processAction = (system) => {
  return {
    // Incoming Invitation.
    doStep: async (msg, _ctx) => {
      console.log(msg.message)
      msg.matrix.acceptConnection(msg.message.roomId)
      msg.database.insertContact({
        room_id: msg.message.roomId,
        did: 'unknown',
        didMethod: 'unknown',
        matrixUser: msg.message.info.matrixUser,
        matrixFederation: ':'+msg.message.info.matrixFederation,
        type: 'contact'
      })
    }
  }
}

module.exports.processAction = processAction
