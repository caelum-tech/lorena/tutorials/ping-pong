const Recipe = require('../lib/lorena-recipe')

// Logger
const Logger = require('../lib/logger')
const logger = new Logger()

const processAction = (system) => {
  return {

    // Process Message.
    doStep: async (msg, _ctx) => {
      const command = msg.message.content.body.split(' ')
      const recipe = command[0].toLowerCase()
      command.shift()
      logger.debug("** MESSAGE TO "+recipe)
      // check if recipe is versioned
      let name  = recipe
      let version = undefined
      if (name.indexOf(":") !== -1) {
        [name, version] = recipe.split(":", 2)
      }

      // Does the service exist?
      const service = msg.register.resolveAction(name, version)

      if (!service) {
        logger.error(`Recipe not found ${recipe}`)
        return 
      }

      const access = await msg.auth.checkAccess(msg.message.contact, service.name, service.version)
      if (!access) {
        logger.error(`Access not permitted to ${recipe}`)
        return 
      }

      if (service) {
        const recipeService = new Recipe(msg.message.info, msg.message.contact, service, msg.database, msg.matrix, msg.blockchain, msg.register, msg.auth)
        // Continue an existing Recipe.
        if (command[0] === 'recipe') {
          const recipeId = parseInt(command[1])
          logger.log("Recipe " + recipeId)
          const recipeInfo = await msg.database.getRecipe(recipeId)
          if (recipeInfo && recipeInfo.status === 'open') {
            command.shift()
            command.shift()
            await recipeService.load(recipeInfo, command)
          }
        } else {
          await recipeService.start(recipe, msg.message.contact, command, 'msg.text')
        }
      } else {
        logger.error(`Recipe ${recipe} not found`)
      } 
    }
  }
}

module.exports.processAction = processAction
