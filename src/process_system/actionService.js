const Recipe = require('../lib/lorena-recipe')
const Logger = require('../lib/logger')
const logger = new Logger()

const processAction = (system) => {
  return {

    // Process Action.
    doStep: async (msg, _ctx) => {
      // TODO: send just message, action & contact. Resulting actions must be processed here : SendMessage to Matrix
      const action = JSON.parse(msg.message.content.body)
      const recipeId = parseInt(action.recipeId)
      const payload = action.payload
      console.log("\n** ACTION ("+recipeId+")"+action.recipe)
      console.log(action)
  
      const service = msg.register.resolveAction(action.recipe)
      if (!service) {
        logger.error(`Recipe not found ${action.recipe}`)
        return 
      }

      const access = await msg.auth.checkAccess(msg.message.contact, service.name, service.version)
      if (!access) {
        logger.error(`Access not permitted to ${action.recipe}`)
        return 
      }
      console.log("EI ")    
      if (recipeId === 0) {
        console.log("Start Action ")
        const recipeService = new Recipe(msg.message.info, msg.message.contact, service, msg.database, msg.matrix, msg.blockchain, msg.register, msg.auth)
        
        // await recipe.start(action.recipe, msg.message.contact.did, action.recipeId, payload)
        await recipeService.start(action.recipe, msg.message.contact, payload, 'msg.action', action.remoteRecipeId, action.remoteRecipe)
        
      } else {
        const recipeInfo = await msg.database.getRecipe(recipeId)
        if (recipeInfo && recipeInfo.status === 'open') {
          const recipeService = new Recipe(msg.message.info, msg.message.contact, service, msg.database, msg.matrix, msg.blockchain, msg.register, msg.auth)
          await recipeService.load(recipeInfo, payload)
        }
      }
    }
  }
}

module.exports.processAction = processAction
