// Init your Fruit Vault (Identity Container)
const {read} = require('readdir');
const Index = require('./lib/lorena-index')

const argv = require('yargs')
  //.scriptName("pirate-parser")
  .usage('$0 <cmd> [args]\n\nArguments can be provided as enviroment vars prefixed by "PDS_"')
  .command("init [didMethod] [name]", 
	'Initialize\n\nex: idspace init did:lor:lab: "Caelum Labs"', 
	(yargs) => {
		yargs.positional("didMethod", {
			describe: "DID method",
			type: "string"
		})
		.positional("name", {
			describe: "Name of the IDSpace",
			type: "string",
		})
		.option("password", {
			describe: "Password",
			type: "string",
		  alias: "p",
		  demandOption: true,
		})
		.demandOption(['didMethod', 'name', 'password'], 'Please provide did method, name and password')
  })
  .command(["run"],
  	"Run node\n\nex: idspace run", 
  	(yargs) => {
		yargs.option("handler", {
			describe: "handler",
			type: "string"
		})
		.option("password", {
		  describe: "Password",
		  type: "string",
		  alias: "p",
		  demandOption: true,
		})
  })
  .command(["addclient [handler]", "addclient "], 
  	"Add a client\n\nex: idspace addclient 9a2ff2824c98f450", 
  	(yargs) => {
		yargs.positional("handler", {
		  describe: "handler",
		  type: "string"
		})
		.option("password", {
		  describe: "Password",
		  type: "string",
		  alias: "p",
		  demandOption: true,
		})
		.demandOption(['handler'], 'Please provide handler and password')
  })
  .demandCommand(1, 'Please select a valid command')
  .env("PDS")
  .help()
  .alias("h", "help")
  .argv;


const init = async (didMethod, name, password) => {
  // Check needed vars to start
  new Index(didMethod)
	.new(name, password)
	.then(() => {
	  console.log('Finished')
	  process.exit(0);
	})
}

const addclient = async (didURL, password) => {
  let did = didURL.split(":")
  let id = did.pop()
  new Index(did.join(":")+':')
	.addClient(id, password)
	.then(() => {
	  console.log('Finished')
	  process.exit(0);
	})
}

const run = async (didURL, password) => {
  let did = didURL.split(":")
  let id = did.pop()
  new Index(did.join(":")+':')
	.run(id, password)
}

const getDID = async(handler) => {
	return new Promise( (resolve,reject) => {
		let exist = false
		did = handler
		read("./data", "*.db", (err, paths) => {
			if (typeof handler === 'string' && handler.length > 0) {
				exist = paths.includes(handler+'.db')
			}
			else if (paths.length > 0) {
				const didFile = paths[0].split('.')
				did = didFile[0]
				exist = true
			}
			if (!exist) {
				reject()
			}
			else {
				resolve(did)
			}
		})
	})
	
}

const command = argv["_"][0]

if (command === "init") {
	init(argv.didMethod, argv.name, argv.password)
}

if (command === "run") {
	getDID(argv.handler)
	.then((did) => {
		run(did, argv.password)
	})
	.catch((e) => {
		console.log("Invalid DID")
	})
}

if (command === "addclient") {
	getDID(argv.handler)
	.then((did) => {
		addclient(did, argv.password)
	})
	.catch((e) => {
		console.log("Invalid DID")
	})

	
}
