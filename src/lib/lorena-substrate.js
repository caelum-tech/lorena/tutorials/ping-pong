'use strict'
const { ApiPromise, WsProvider, Keyring } = require('@polkadot/api')

// Zenroom
// const Zen = require('@caelum-tech/zenroom-lib')
// const z = new Zen()

// Logger
const Logger = require('./logger')
const logger = new Logger()

/**
 * Javascript Class to interact with the Blockchain.
 */
module.exports = class Blockchain {
  constructor () {
    this.providerWS = process.env.SERVER_SUBSTRATE || 'ws://localhost'
    this.api = false
    this.keypair = {}
    this.units = 1000000000
  }

  async connect () {
    logger.log('connecting')

    const provider = new WsProvider(this.providerWS)
    this.api = await ApiPromise.create({ provider })

    const [chain, nodeName, nodeVersion] = await Promise.all([
      this.api.rpc.system.chain(),
      this.api.rpc.system.name(),
      this.api.rpc.system.version()
    ])

    /* let kZpair = await z.newKeyPair('root')
    console.log(kZpair)

    const keyring = new Keyring({ type: 'sr25519' });
    const newPair = keyring.addFromUri('//'+kZpair['root'].keypair.private_key);
    //
    const hexPair = keyring.addFromUri('0x80a0c8d8a5e27c75caa472fdcac6e24699a75144966d041f5909c4dc0e970b71');
    console.log(newPair.address)

    // Retrieve the account balance via the balances module
    const balance = await this.api.query.balances.freeBalance(hexPair.address)
    console.log("Balance "+balance) */

    logger.log(`You are connected to chain ${chain} using ${nodeName} v${nodeVersion}`)
  }

  /**
   *
   * @param {string} seed Seed
   * @param {boolean} isSeed Seed ot URI
   */
  setKeyring (seed, isSeed = false) {
    const keyring = new Keyring({ type: 'sr25519' })
    const uri = ((isSeed) ? '' : '//') + seed
    this.keypair = keyring.addFromUri(uri)
  }

  balance () {
    return new Promise(async (resolve) => {
      const balance = await this.api.query.balances.freeBalance(this.keypair.address)

      resolve(balance / this.units)
    })
  }

  async transfer (to, total) {
    return new Promise(async (resolve, reject) => {
      const ADDR = to
      const AMOUNT = total * this.units

      const balance = await this.api.query.balances.freeBalance(this.keypair.address)
      const nonce = await this.api.query.system.accountNonce(this.keypair.address)

      if (balance > AMOUNT) {
        this.api.tx.balances
          .transfer(ADDR, AMOUNT)
          .signAndSend(this.keypair, { nonce }, async ({ events = [], status }) => {
            if (status.isFinalized) {
              console.log('Blockchain Transfer complete')
              resolve()
            }
          })
      }
    })
  }
}
