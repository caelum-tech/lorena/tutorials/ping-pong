// DID
const DID = require('./lorena-did')

// Logger
const Logger = require('./logger')
const logger = new Logger()
const dotenv = require('dotenv')
dotenv.config()

module.exports = class Index {
  /**
   * Constructor
   *
   * @param {string} didMethod DID method
   */
  constructor (didMethod) {
    this.shuttingDown = false
    this.did = new DID(didMethod)
  }

  /**
   * Shut down Index processing
   */
  async shutdown () {
    this.shuttingDown = true
  }

  /**
   * Listen to events
   *
   * @param {number} times Listen to events n times
   */
  async start (times) {
    return new Promise(async (resolve, reject) => {
      // Handle events.
      while (true) {
        await this.did.listen()
        if (this.shuttingDown) {
          break
        }
      }
      resolve()
    })
  }

  /**
   * Creates a new DID Object
   */
  async new (name, password) {
    await this.did.new(name, password)

    // for organizations we need to open a channel with root and get tokens.
    if (this.handler !== 'root') {
      // return this.start(3, password)
    } else {
      return Promise.resolve()
    }
  }

  /**
   * Opens a DID Object
   *
   * @param {number} times to run
   */
  async run (did, password) {
    await this.did.open(did, password)
    this.start()
      .then(() => {
        logger.log('index.js main exiting.')
      })
  }

  /**
   * Adds a device.
   *
   * @param {string} cliHandler Device Handler to be added (only Matrix by now)
   */
  async addClient (did, password) {
    return new Promise(async (resolve) => {
      await this.did.open(did, password)
      await this.did.addClient()
      resolve()
    })
  }
}
