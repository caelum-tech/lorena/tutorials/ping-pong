// needs connection with : SQLITE, Matrix, IPFS & Zenroom
const DB = require('./db/lorena-db-sqlite')

// Matrix
const Matrix = require('./lorena-matrix')

// IPFS
const IPFS = require('./lorena-ipfs')
const ipfs = new IPFS()

// Zenroom
const Zen = require('@caelum-tech/zenroom-lib')
const z = new Zen()

const Register = require('./lorena-register')
const Authorization = require('./lorena-recipe-auth')

// Substrate
const Blockchain = require('./lorena-substrate')

// Import DID Document
const diddoc = require('@caelum-tech/caelum-diddoc-nodejs')

// Logger
const Logger = require('./logger')
const logger = new Logger()

// Nact.
// Actors to interact with Matrix
const { start, stop } = require('nact')
// const Actors = require('../actors/actors')

var path = require('path')

/**
 * Javascript Class to interact with IPFS for Lorena.
 */
module.exports = class DID {
  /**
   * Creates a DID Object
   *
   * @param {string} handler Did Handler
   * TODO : Check handler format (just letters and numbers )
   */
  constructor (didMethod) {
    this.database = false
    this.blockchain = false
    this.actors = []

    // Space Info.
    this.info = {
      name : '',
      did: '',
      didMethod : didMethod,
      matrisUser : '',
      matrixFederation : ':matrix.caelumlabs.com',
      kZpair : false
    }


    // Init Actors in the system.
    this.system = start()
  }


  async initRegister () {
    this.register = new Register(this.system, this.database, true)
    this.loadActors('process_system')
    this.loadActors('process_installed')
  } 

  async initAuth () {
    this.auth = new Authorization(this.register, this.database)
  } 

  /**
   * Load all available recipes.
   *
   * @param {string} actorsPath Where recipes live.
   */
  loadActors (actorsPath) {
    logger.title('Recipes',actorsPath)
    this.register.loadActors(path.join(__dirname, '..', actorsPath)); 
  }

  /**
   * Database - Open sqlite3 ID database
   *
   * @returns {Promise} result
   */
  async openDB (did) {
    logger.title('Sqlite', 'Open DB')
    return new Promise(async (resolve, reject) => {
      try {
        this.database = await new DB(did)
        await this.database.init()
        resolve()
      } catch (e) {
        console.log(e)

        /* istanbul ignore next */
        reject(e)
      }
    })
  }

  /**
   * Open Blockchain connection
   *
   * @returns {Promise} result
   */
  async openBlockchain () {
    logger.title('Blockchain', 'Open Connection')
    return new Promise(async (resolve, reject) => {
      try {
        this.blockchain = new Blockchain()
        await this.blockchain.connect()
        this.blockchain.setKeyring(this.info.kZpair[this.handler].keypair.private_key)
        logger.key('Address', this.blockchain.keypair.address)
        resolve()
      } catch (e) {
        /* istanbul ignore next */
        reject(e)
      }
    })
  }

  /**
   * Inits Matrix user.
   *
   * @param {string} password Did Password
   * @returns {Promise} result
   */
  async initComms (password) {
    logger.title('Matrix', 'Connect User')
    return new Promise(async (resolve, reject) => {
      this.matrix = new Matrix()

      resolve()
    })
  }

  /**
   * Adds a Matrix user.
   *
   * @param {string} matrixUser User handler
   * @param {string} password Did Password
   * @returns {Promise} result
   */
  async newCommsUser (matrixUser, password) {
    logger.key('New matrix user', matrixUser)
    return new Promise(async (resolve) => {
      // Check if user already exist, if not create it.
      if (await this.matrix.available(matrixUser)) {
        logger.debug("Registering matrix user:" + matrixUser)
        await this.matrix.register(matrixUser, password)
      } else {
        logger.debug("Matrix user " + matrixUser + " not available")
      }
      resolve()
    })
  }

  /**
   * Logins a Matrix user.
   *
   * @param {string} handler User handler
   * @param {string} password Did Password
   * @returns {Promise} result
   */
  async loginCommsUser (matrixUser, password) {
    return new Promise(async (resolve, reject) => {
      logger.key('Login matrix user', matrixUser)
      // TODO: When login fails (workng password), fail in a more elegant way.
      // Try to login.
      try {
        const token = await this.matrix.connect(matrixUser, password)
        // TODO: No neeed to store token in the database. Use in memory instead.
        await this.database.set('matrix_token', token)
        resolve()
      } catch (e) {
        console.log(e)
        reject('Could not connect to Matrix')
      }
    })
  }

  /**
   * Init Zenroom
   *
   * @returns {Promise} of Zenroom key creation and database set
   * @param {string} handler Handler to init Zenroom
   */
  async initZenroom (name) {
    return new Promise(async (resolve, reject) => {
      try {

        logger.title('Zenroom', 'new DID & keypair')
        
        // Calculate DID.
        //TODO: Add a Random number in zenroom for DID
        let zhash  = await z.hash(name)
        this.info.did = zhash.hash.substr(0,16)
        this.info.kZpair = await z.newKeyPair(this.info.did)
        logger.key('Zenroom', 'kZpub', this.info.kZpair[this.info.did].keypair.public_key)

        // Get matrixuser
        // TODO: random number instead
        this.info.matrixUser = this.info.did

        resolve()
      } catch (e) {
        /* istanbul ignore next */
        reject(e)
      }
    })
  }

  /**
   * SaveDIDObject
   *
   * @returns {Promise} of new diddoc in IPFS
   **/
  async saveDIDObject () {
    logger.title('Blockchain - IPFS', 'Connect')
    return new Promise(async (resolve, reject) => {
      try {
        const didDocument = new diddoc
          .DidDoc('https://www.w3.org/ns/did/v1', this.info.did)
          .addAuthentication({
            id: this.info.did + '#zenroom',
            type: 'Zenroomv1',
            controller: this.info.did + '#zenroom',
            publicKey: '-- Public Key --' // this.info.kZpair.root.keypair.public_key.toString()
          })
          .addService({
            id: this.info.did + '#ping',
            type: 'LorenaService',
            serviceEndpoint: '@'+this.info.did+this.info.matrixFederation
          })

        logger.log('DIDDoc created')
        const file = await ipfs.add(didDocument.toJSON())
        await this.database.set('ipfs', file[0].path)
        logger.key('IPFS', file[0].path)
        resolve()
      } catch (e) {
        /* istanbul ignore next */
        reject(e)
      }
    })
  }

  /**
   * Creates or loads the DID
   *
   * @param {string} password Did Password
   * @returns {Promise} of initialized database, zenroom, and DID object
   */
  async new (name, password) {
    return new Promise(async (resolve) => {

      // Create DID
      this.initZenroom(name)
        // Create & Open Database.
        .then(() => { return this.openDB(this.info.did) })
        // Create Did Document and save to IPFS
        // .then(() => { return this.saveDIDObject() })
        // Init Comms and create Matrix User.
        .then(() => { return this.initComms() })
        .then(() => { return this.newCommsUser(this.info.did, password) })
        .then(() => {  return this.loginCommsUser(this.info.did, password) })
        .then(async () => {
          logger.title('DID', 'Basic Settings')
          let contactId = await this.database.insertContact({
            room_id: '',
            did: this.info.did,
            didMethod: this.info.didMethod,
            matrixUser: this.info.matrixUser,
            matrixFederation: this.info.matrixFederation,
            createdBy: this.info.didMethod+this.info.did,
            type: 'me'
          })
          await this.database.addKeys(contactId, this.info.kZpair)
          // await this.database.set('kZpair', this.info.kZpair, 'J')
          await this.database.set('name', name)
          await this.database.set('next_batch', '')
          this.nextBatch = ''
          resolve(this)
        })
        .catch((err) => {
          this.database.close()
          logger.error(err)
        })
    })
  }

  /**
   * Gets a Diddocument from IPFS
   *
   * @param {string} password for matrix
   * @returns {Promise} result
   */
  open (did, password) {

    return new Promise(async (resolve, reject) => {
      this.openDB(did)
      .then(() => {
        return this.initRegister() 
      })
      .then(() => {
        return this.initAuth() 
      })
      .then(async () => { 
        this.info = await this.database.getContact(did)
        this.nextBatch = await this.database.get('next_batch')
        return this.initComms() 
      })
      .then(() => { return this.loginCommsUser(this.info.matrixUser, password) })
      .then( async () => { 
        // await this.openBlockchain()
        logger.title('PDS - Lorena - ' + this.info.did, 'Listening...')
        resolve()
      })
      .catch( (err) => {
        /* istanbul ignore next */
        reject(err)
      })
    })
}

  /**
   * Close everything
   */
  close () {
    stop(this.system)
    this.database.close()
  }

  /**
   * Adds a client to the list of clients.
   *
   * @param {string} cliHandler Client Handler to be added
   * @param {string} password Password for the client
   * @returns {Promise} result of adding a Client.
   */
  addClient () {
    return new Promise(async (resolve ) => {
      // Create client Matrix user. Random user and password
      let rnd = await z.newKeyPair('rnd')
      let zhash  = await z.hash(rnd.rnd.keypair.public_key) 
      const cliMatrix  = zhash.hash.substr(0,16)
      
      rnd = await z.newKeyPair('rnd')
      zhash  = await z.hash(rnd.rnd.keypair.public_key)
      const passMatrix = zhash.hash.substr(0,16)

      rnd = await z.newKeyPair('rnd')
      zhash  = await z.hash(rnd.rnd.keypair.public_key)
      const roomName = zhash.hash.substr(0,16)

      // CLient CODE used to connect
      logger.key('Client CODE', cliMatrix+'-'+passMatrix+'-'+this.info.did)
      
      this.newCommsUser(cliMatrix, passMatrix)
      .then(() => {
        // Send invitation to connect.
          return this.matrix.createConnection(this.info.did, this.info.didMethod, roomName, cliMatrix, this.info.matrixFederation, 'client')
        })
        .then(async (contact) => {
          await this.database.insertContact(contact)

          // Connect as client and accept invitation.
          await this.loginCommsUser(cliMatrix, passMatrix)
          await this.matrix.acceptConnection(contact.room_id)
          resolve()
        })
    })
  }

  async listen () {
    const context = { 
        register: this.register, 
        auth: this.auth, 
        database: this.database, 
        info: this.info, 
        blockchain: this.blockchain
    }
    this.nextBatch = await this.matrix.events(context, this.nextBatch)
    await this.database.set('next_batch', this.nextBatch)
  }
}
