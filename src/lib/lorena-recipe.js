'use strict'
const { dispatch } = require('nact')

/**
 * Return an Object if it's a JSON and a string otherwise.
 *
 * @param {string} str String to parse.
 * @returns {object} Parsed JSON.
 */
function jsonParse (str) {
  try {
    return JSON.parse(str)
  } catch (e) {
    return str
  }
}

/**
 * Javascript Class to interact with Zenroom.
 */
module.exports = class Recipe {
  constructor (info, contact, service, database, matrix, blockchain, register, auth) {
    this.info = info
    this.contact = contact
    this.service = service
    this.database = database
    this.matrix = matrix
    this.blockchain = blockchain
    this.register = register
    this.auth = auth
    this.arguments = []
    this.recipeInfo = {}
  }

  /**
   * Load arguments from Payload.
   *
   * @param {Array} payload Payload with arguments as an array
   * @returns {boolean} False if an error has occurred. True otherwise.
   */
  async loadArguments (payload) {
    let argstxt = []
    if (Array.isArray(payload)) {
      // Info coming from CLI.
      argstxt = payload
    } else {
      // Info coming from another DID.
      Object.keys(payload).forEach(function (key) {
        argstxt.push(key)
        argstxt.push(payload[key])
      })
    }
    const args = argstxt
    const neededArgs = (this.service.args) ? parseInt(this.service.args.length) : 0
    const sentArgs = (args) ? (parseInt(args.length) / 2) : 0
    let leftArgs = neededArgs
    let did = ''
    let contact = {}

    // Check number of arguments.
    if (neededArgs !== sentArgs) {
      return false
    } else if (neededArgs > 0) {
      // Collect all arguments

      
      for (let i = 0; i < neededArgs; i++) {
        // Identify argument.
        if (this.service.args.includes(args[0])) {
          leftArgs--
          // Collect arg and value. Clean args array.
          const arg = args[0]
          args.shift()
          const value = args[0]
          args.shift()

          // If it's a did. Load contact.
          switch (arg) {
            case 'did':
              contact = await this.database.getContact(value)
              if (contact) {
                this.arguments.did = { ...contact, connected: true }
              } else {
                this.arguments.did = { did: value, connected: false }
              }
              break
            case 'bank':
              contact = await this.database.getContact(value)
              if (contact) {
                this.arguments.bank = { ...contact, connected: true }
              } else {
                this.arguments.bank = { did: did, connected: false }
              }
              break
            case 'total':
              this.arguments[arg] = parseInt(value)
              break
            default:
              this.arguments[arg] = value
              break
          }
        } else {
          return false
        }
      }
    }
    if (leftArgs > 0) {
      return false
    } else {
      return true
    }
  }

  /**
   * Load an existing Recipe.
   *
   * @param {object} recipeInfo All info on the Recipe
   * @param {string} payload Payload
   */
  async load (recipeInfo, payload) {
    return new Promise(async (resolve, reject) => {
      this.recipeInfo = recipeInfo
      this.recipeInfo.steps = await this.database.getSteps(this.recipeInfo.recipeId)
      this.recipeInfo.totalSteps = this.service.steps.length
      this.recipeInfo.currentPayload = payload
      dispatch(this.service.doStep, this)
      resolve()
    })
  }

  /**
   * Start one recipe.
   *
   * @param {string} recipeType Recipe Type
   * @param {string} createdBy Who created this recipe
   * @param {number} createdByRecipeId If this recipe was started by another remote recipe
   * @param {string} payload Payload
   */
  async start (recipeType, contact, payload, msgType='msg.action', remoteRecipeId = 0, remoteRecipe = '') {
    return new Promise(async (resolve, reject) => {
      this.recipeInfo.currentStep = 1
      this.recipeInfo.recipe = recipeType
      this.recipeInfo.contactId = contact.id
      this.recipeInfo.totalSteps = this.service.steps.length
      // Process arguments.
      // TODO: Break if errors.
      const ok = await this.loadArguments(payload)
      if (!ok) {
        reject()
      } else {
        // Create Recipe on DB.
        let sql = 'INSERT INTO recipes (recipe, contactId) VALUES (\'' + recipeType + '\','+ contact.id+')'
        this.recipeInfo.recipeId = await this.database.runSQL(sql)
        this.recipeInfo.createdByRecipeId = remoteRecipeId
        

        sql = 'INSERT INTO steps (step, recipeId, remoteRecipeId, remoteRecipe, type, msgType, error, contactId, status, closedAt, payload) VALUES (' + 
        '0,' + this.recipeInfo.recipeId + ','+ remoteRecipeId+','+ '\''+ remoteRecipe +'\''+',\'start\',\''+msgType+'\',\'\',' + contact.id + ', \'done\', CURRENT_TIMESTAMP, \''+payload+'\')'
        await this.database.runSQL(sql)

        // Add steps to Recipe.
        for (let i = 0; i < this.recipeInfo.totalSteps; i++) {
          const step = this.service.steps[i]
          let contactId = 0

          // Send to another DID (not the DID who originated the recipe)
          if (step.contact === 'did' && this.arguments.did) {
            contactId = this.arguments.did.id
          } else if (step.contact === 'bank' && this.arguments.bank) {
            contactId = this.arguments.bank.id
          } else {
            // Origin DID.
            contactId = contact.id
          }
          remoteRecipe = (this.service.steps[i].recipe) ? this.service.steps[i].recipe : ''
          const error = (this.service.steps[i].error) ? this.service.steps[i].error : ''
          sql = 'INSERT INTO steps (step, recipeId, remoteRecipe, type, msgType, error, contactId) VALUES (' + 
            (i + 1) + ',' + this.recipeInfo.recipeId + ','+ '\''+ remoteRecipe +'\''+
            ',\'' + this.service.steps[i].type + '\',\''+msgType+'\',\'' + error + '\',' + contactId + ')'

            await this.database.runSQL(sql)
        }

        // Load steps to local Class.
        this.recipeInfo.steps = await this.database.getSteps(this.recipeInfo.recipeId)
        dispatch(this.service.doStep, this)
        resolve(this.recipeInfo.recipeId)
      }
    })
  }

  /**
   * Move to Next Step.
   */
  async nextStep () {
    return new Promise(async (resolve) => {
      // Update step. Move next.
      let payload = (this.recipeInfo.currentPayload) ? this.recipeInfo.currentPayload : ''
      payload = (typeof payload === 'string') ? payload : JSON.stringify(payload)
      let sql = 'UPDATE steps SET status=\'done\', payload=\'' + payload + '\', closedAt=CURRENT_TIMESTAMP WHERE recipeId=' + this.recipeInfo.recipeId + ' AND step=' + this.recipeInfo.currentStep
      await this.database.runSQL(sql)
      this.recipeInfo.currentStep++
      this.recipeInfo.currentPayload = ''

      // Update recipes.
      sql = 'UPDATE recipes SET currentStep=\'' + this.recipeInfo.currentStep + '\' WHERE recipeId=' + this.recipeInfo.recipeId
      await this.database.runSQL(sql)

      resolve()
    })
  }

  /**
   * Move to step without executing.
   *
   * @param {*} stepTo Step we want to jump to
   */
  async jumpToStep (stepTo) {
    while (this.recipeInfo.currentStep < stepTo) {
      await this.nextStep()
    }
  }

  /**
   * Close Recipe.
   */
  async close () {
    return new Promise(async (resolve) => {
      // Update step. Move next.
      const sql = 'UPDATE recipes SET status=\'closed\', closedAt=CURRENT_TIMESTAMP WHERE recipeId=' + this.recipeInfo.recipeId
      await this.database.runSQL(sql)
      resolve()
    })
  }

  /**
   * Send message in this step.
   *
   * @param {object} step Current Step.
   * @param {*} payload Additional Payload to send.
   */
  async send (step, payload) {
    return new Promise(async (resolve) => {
      const contact = await this.database.getContactById(step.contactId)
      // Initial parameters.
      let sendPayload = payload
      // If we are tolking to another IDSpace.
      if (step.msgType === 'msg.action') {
        const recipe = (step.type === "start") ? step.remoteRecipe : this.recipeInfo.steps[step.stepFrom].remoteRecipe
        const recipeId = (step.type !== "start") ? this.recipeInfo.steps[step.stepFrom].remoteRecipeId : step.remoteRecipeId

        sendPayload = JSON.stringify({
          recipe: recipe,
          recipeId: recipeId,
          remoteRecipe: this.recipeInfo.recipe,
          remoteRecipeId: this.recipeInfo.recipeId,
          payload: payload
        })
        console.log("********************")
      }
      // Pretty formatting objects for text channels
      if ((step.msgType === 'msg.text') && (typeof sendPayload != "string")) {
        sendPayload = JSON.stringify(sendPayload, null, 2)
      }
      //TODO: Allow messages via RIOT : format=m.text
      await this.matrix.sendMessage(contact.roomId, step.msgType, sendPayload)
      resolve()
    })
  }

  /**
   * Get Payload from a Step
   *
   * @param {number} step Step number
   * @returns {Promise} payload
   */
  async getPayload (step) {
    return new Promise(async (resolve) => {
      const res = await this.database.getSQL('SELECT payload FROM steps WHERE recipeId=\'' + this.recipeInfo.recipeId + '\' AND step=\'' + step + '\'')
      let ret = (res) ? jsonParse(res.payload) : ''
      if ( ret.payload ){
        ret = JSON.parse(ret.payload)
      }
      resolve(ret)
    })
  }

  /**
   * Get Payload from a Step
   *
   * @param {string} type Credential type
   * @param {string} issuer DID of the issuer
   * @returns {Promise} payload
   */
  async getCredential (type, issuer) {
    return new Promise(async (resolve) => {
      const res = await this.database.getSQL('SELECT * FROM credentials WHERE credentialType=\'' + type + '\' AND createdBy=\'' + issuer + '\'')
      resolve(res)
    })
  }

  /**
   * Executes next Step
   *
   * @param {string} payload Payload
   * @returns {Promise} Next step executed
   */
  async next (payload = false) {
    // Automatically jump to last step.
    let callAgain = false
    let cred = {}
    let sql = ''

    return new Promise(async (resolve) => {
      const step = this.recipeInfo.steps[this.recipeInfo.currentStep]
      switch (step.type) {
        case 'receive':
          await this.nextStep()
          callAgain = true
          break
        case 'receive_credential':
          cred = this.recipeInfo.currentPayload
          sql = 'INSERT INTO credentials (keyId,credentialIndex,credentialType,createdBy,createdByRecipe,credentialSubject,credentialProof,status) VALUES (' +
            cred.credentialSubject.keyIndex + ',' +
            cred.credentialSubject.credentialId + ',' +
            '\'' + cred.type[0] + '\',' +
            '\'' + cred.credentialSubject.issuer + '\',' +
            this.recipeInfo.recipeId + ',' +
            '\'' + JSON.stringify(cred.credentialSubject) + '\',' +
            '\'' + JSON.stringify(cred.proof) + '\',' +
            '\'active\')'
          await this.database.runSQL(sql)
          await this.nextStep()
          callAgain = true
          break
        case 'send':
        case 'start':
          await this.send(step, payload)
          await this.nextStep()
          break
      }

      // Close Recipe.
      if (this.recipeInfo.currentStep > this.recipeInfo.totalSteps) {
        await this.close()
      }

      // Call next step?
      if (callAgain) {
        dispatch(this.service.doStep, this)
      }
      resolve()
    })
  }
}
