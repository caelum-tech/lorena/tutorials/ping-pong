const path = require('path');
const fs = require('fs');
const { spawnStateless } = require('nact')

// Logger
const Logger = require('./logger')
const logger = new Logger()

/**
  * Reports errors productd in function
  *
  */
function errorReportingFunction (f) {
  const decoratedFunction = async function () {
    try {
      return await f(...arguments)
    } catch (e) {
        logger.error(e)
        logger.debug(e.stack)
        throw e
    }
  }

  return decoratedFunction
}


/** 
  * Actor encapsulation class
  *
  */

class Actor {
    constructor (name, version, action, active) {
      this.name = name
      this.version = version
      this.action = action
      this.description = this.action.description
      this.active = active
    }
}

/**
 * Javascript Class that represents an ACtor.
 */

module.exports = class Register {
  /**
   * Create then recipes register
   *
   * 
   */
  constructor (system, database, hotreload=true) {
      this.actors = [];
      this.names = {};
      this.system = system
      this.database = database
      this.hotreload = hotreload
  }

  /** 
    * Load actos from path
    */

  async loadActor(file, reload) {

    if (reload) {
      delete require.cache[require.resolve(file)]
    }

    let actorCode
    
    try {
      actorCode = require(file)
    } catch (e) {
      logger.error(`Errors loading actor ${file}`)
      logger.debug(e)
      return 
    }
    const actorAction = actorCode.processAction(this.system)

    const filename = path.basename(file)

    // The name is on the code or the name of the file
    let name = filename.substring(0, filename.length - 3);
    if (actorAction.name) {
      name = actorAction.name
    } 

    const version = actorAction.version

    // the full name is the anme with the version if its informes
    let fullname = name
    if (version) {
      fullname = fullname + "_v" + version.replace(/\./, "_") 
    }

    actorAction.doStep = spawnStateless(this.system, errorReportingFunction(actorAction.doStep) /*, fullname*/)

    const active = await this.isSavedActive(name, version)

    const actor = new Actor(name, version, actorAction, active);

    this.registerActor(actor, name, version)

    logger.key('Loaded actor ' + fullname + ' from ', file)

    return actor;
  }

  loadActors(path) {
      fs.readdir(path, (err, files) => {
        if (err) {
          logger.error(err);
          return;
        }
        files.forEach(async (filename) => {
          if (!filename.endsWith(".js")) {
            logger.log("Ignoring file " + filename);
            return;
          }
          const filepath = path + "/" + filename 
          await this.loadActor(filepath)
          if (this.hotreload) {
            this.watchFile(filepath)
          }
        })
      });
  }

  watchFile(file) {
      fs.watchFile(file, (curr, prev) => {
        console.log(file + " file changed, reloading")
        this.loadActor(file, true)
      });      
  }

  registerActor(actor, name, version) {
    // check if actor is already registered
    const existent = this.resolve(name, version)
    if (existent) {
      this.actors.splice(this.actors.indexOf(existent),1)
    }
    this.actors.push(actor)
  }

  getRegisteredActors () {
    return this.actors.slice();
  }

  /**
   * Resolve revipe by name and optional version
   *
   * @param {string} name of the reipe.
   * @param {string} version, if not provided resolve the major version.
   * @param {boolean} If true resolve only active recipes
   * @returns {Actor} Actor object of the resolved actor
   */
  resolve (name, version, active) {
    let selected;
    this.actors.forEach((actor) => {
      // select only active or all
      if (active && !actor.active) {
        return
      }
      if (actor.name === name) {
        if (version) {
          // if version is specified only get exact version
          if (actor.version === version) {
              selected = actor
          }
        } else {
          // if version is not specified, get last version
          if (!selected) {
            selected = actor
          }
          if (actor.version > selected.version) {
            selected = actor
          }
        }
      }
    })
    return selected
  }

  resolveAction (name, version) {
    const actor = this.resolve(name, version, true);
    if (actor) {
      return actor.action
    }
  }

  /**
   *  Get from database if service is enabled
   *
   *  This permits persisiting the enabled state between restarts
   *  If not found on database, consider it enabled
   */
  async isSavedActive (name, version) {
      version = version || ''
      const res = await this.database.getSQL(`SELECT active FROM register WHERE name='${name}' AND version='${version}'`)
      if (!res) {
        return true
      } else {
        return !!res.active
      }
  }

  async saveActive (name, version, active) {
      version = version || ''
      const res = await this.database.getSQL(
        `SELECT * FROM register WHERE name='${name}' AND version='${version}'`)
      if (!res) {
        await this.database.runSQL(
          `INSERT INTO register (name, version, active) VALUES ('${name}', '${version}', ${active})`)
      } else {
        await this.database.runSQL(
          `UPDATE register SET active=${active} WHERE name='${name}' AND version='${version}'`)
      }
  }

  async enable (name, version) {
    const actor = this.resolve(name, version)
    if (actor) {
      actor.active = true
      await this.saveActive(actor.name, actor.version, actor.active)
    }
    return actor
  }

  async disable (name, version) {
    const actor = this.resolve(name, version)
    if (actor) {
      actor.active = false
      await this.saveActive(actor.name, actor.version, actor.active)
    }
    return actor
  }

} 
