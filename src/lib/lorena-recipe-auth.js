// Logger
const Logger = require('./logger')
const logger = new Logger()


/**
 * Javascript Class that represents an ACtor.
 */

module.exports = class Authorization {
  /**
   * Create then recipes register
   *
   * 
   */
  constructor (register, database) {
      this.register = register
      this.database = database

      this.grants = []
  }

  async checkAccess(contact, name, version) {
      if (contact.type == 'me') {
        return true
      }
      // SUPER UGLY 
      if (contact.id == 2) {
        return true
      }
      // By now access alway permitted
      return true
      const contactId = contact.id
      const key = `${contactId}-${name}-${version}`
      return !!this.grants[key]
  }

  async grant(contactId, name, version) {
      const key = `${contactId}-${name}-${version}`
      this.grants[key] = 'grant'
  }

  async revoke(contactId, name, version) {
      const key = `${contactId}-${name}-${version}`
      this.grants[key] = undefined
  }
} 
