const ipfsClient = require('ipfs-http-client')

// Logger
const Logger = require('./logger')
const logger = new Logger()

/**
 * Javascript Class to interact with IPFS for Lorena.
 */
module.exports = class IPFS {
  /**
   * Connects to IPFS node
   *
   * @param {string} serverURL IPFS server node URL, default 'http://localhost:5001'
   */
  constructor (serverURL = process.env.SERVER_IPFS || 'http://localhost:5001') {
    this.ipfs = ipfsClient(serverURL)
  }

  /**
   * Detects connection to a live IPFS node
   *
   * @returns {boolean} result
   */
  async connected () {
    let id
    try {
      id = await this.ipfs.id()
      return (id.id && id.id !== '')
    } catch (err) {
      /* istanbul ignore next */
      logger.error(err)
      /* istanbul ignore next */
      return false
    }
  }

  /**
   * Adds the Diddocument to IPFS
   *
   * @param {string} diddocument document
   * @returns {Promise} of adding document to IPFS
   */
  async add (diddocument) {
    return new Promise((resolve, reject) => {
      const buffer = Buffer.from(JSON.stringify(diddocument))
      this.ipfs.add(buffer, function (err, file) {
        /* istanbul ignore if */
        if (err) {
          logger.error(err)
          reject(err)
          return
        }
        resolve(file)
      })
    })
  }

  /**
   * Gets a Diddocument from IPFS
   *
   * @param {string} file to get
   * @returns {Promise} of file from IPFS
   */
  async get (file) {
    return new Promise((resolve, reject) => {
      this.ipfs.get(file, function (err, files) {
        /* istanbul ignore if */
        if (err) {
          logger.error(err)
          reject(err)
          return
        }
        resolve(JSON.parse(files[0].content.toString()))
      })
    })
  }
}
