'use strict'

/**
 * Javascript Class to interact with Database.
 */
module.exports = [
    `CREATE TABLE IF NOT EXISTS settings (
        key TEXT PRIMARY KEY,
        type CHAR(1) NOT NULL DEFAULT ('S'),
        value TEXT NOT NULL);`,
    `CREATE TABLE IF NOT EXISTS contacts (
        id INTEGER PRIMARY KEY,
        did TEXT,
        didMethod TEXT,
        matrixUser TEXT,
        matrixFederation TEXT,
        roomId TEXT,
        createdAt DATETIME DEFAULT CURRENT_TIMESTAMP,
        joinAt DATETIME,
        leaveAt DATETIME,
        status TEXT,
        type TEXT,
        level NOT NULL DEFAULT(0)
    );`,
    `CREATE TABLE IF NOT EXISTS recipes (
        recipeId INTEGER PRIMARY KEY,
        recipe TEXT,
        contactId INTEGER,
        currentStep INTEGER DEFAULT(1),
        createdAt DATETIME DEFAULT CURRENT_TIMESTAMP,
        closedAt DATETIME,
        status TEXT DEFAULT('open')
    );`,
    `CREATE TABLE IF NOT EXISTS steps (
        stepId INTEGER PRIMARY KEY,
        step INTEGER,
        stepFrom INTEGER DEFAULT(0),
        recipeId INTEGER,
        remoteRecipeId INTEGER DEFAULT(0),
        remoteRecipe INTEGER,
        contactId INTEGER,
        msgType TEXT,
        payload TEXT,
        createdAt DATETIME DEFAULT CURRENT_TIMESTAMP,
        closedAt DATETIME,
        type TEXT,
        error TEXT,
        status TEXT DEFAULT ('open')
    );`,
    `CREATE TABLE IF NOT EXISTS credentials (
        credentialId INTEGER PRIMARY KEY,
        keyId INTEGER,
        credentialIndex INTEGER,
        credentialType TEXT,
        createdBy TEXT,
        createdByRecipe INTEGER,
        createdAt DATETIME DEFAULT CURRENT_TIMESTAMP,
        credentialSubject TEXT,
        credentialProof TEXT,
        status TEXT
    );`,
    `CREATE TABLE IF NOT EXISTS keys (
        keyId INTEGER PRIMARY KEY,
        contactId INTEGER,
        keyNumber INTEGER,
        createdAt DATETIME DEFAULT CURRENT_TIMESTAMP,
        rotatedAt DATETIME,
        status TEXT,
        key TEXT
    );`,
    `CREATE TABLE IF NOT EXISTS authorization (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT,
        version TEXT,
        contactId INTEGER,
        status TEXT DEFAULT ('granted'),
        createdAt DATETIME DEFAULT CURRENT_TIMESTAMP
    );`,
    `CREATE TABLE IF NOT EXISTS register (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT,
        version TEXT,
        active BOOLEAN DEFAULT 1,
        createdAt DATETIME DEFAULT CURRENT_TIMESTAMP
    );`,
]
