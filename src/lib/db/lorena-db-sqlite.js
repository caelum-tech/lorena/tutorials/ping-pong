'use strict'
const sqlite3 = require('sqlite3').verbose()
const DBInterface = require('./lorena-db')
const DBTables = require('./lorena-db-tables')
const Logger = require('../logger')
const logger = new Logger()

/**
 * Javascript Class to interact with Database.
 */
module.exports = class DB extends DBInterface {
  constructor (did) {
    super()
    this.db = {}
    this.dbPath = `data/${did}.db`
    return new Promise((resolve) => {
      // TODO: encrypt file with password
      this.db = new sqlite3.Database(this.dbPath, (err) => {
        /* istanbul ignore if */
        if (err) {
          console.error(err.message)
          process.exit(2)
        }
        logger.log(`Database Open ${did}`)
        resolve(this)
      })
    })
  }

  /**
   * Runs a SQL query
   *
   * @param {string} sql SQL query
   * @returns {Promise} SQL query result
   */
  runSQL (sql) {
    return new Promise((resolve, reject) => {
      // console.log('SQL ==> ' + sql)
      this.db.run(sql, function (err) {
        /* istanbul ignore if */
        if (err) {
          console.log('ERR ' + err)
          reject(err)
          return
        }
        resolve(this.lastID)
      })
    })
  }

  /**
   * Runs a SQL query
   *
   * @param {string} sql SQL query
   * @returns {Promise} SQL query result
   */
  getSQL (sql) {
    return new Promise((resolve, reject) => {
      // console.log('SQL ==> ' + sql)
      this.db.get(sql, (err, res) => {
        /* istanbul ignore if */
        if (err) {
          reject(err)
          return
        }
        resolve(res)
      })
    })
  }

  /**
   * Runs a SQL query
   *
   * @param {string} sql SQL query
   * @returns {Promise} SQL query result
   */
  allSQL (sql) {
    return new Promise((resolve, reject) => {
      // console.log('SQL ==> ' + sql)
      this.db.all(sql, [], (err, res) => {
        /* istanbul ignore if */
        if (err) {
          reject(err)
          return
        }
        resolve(res)
      })
    })
  }

  /**
   * Set the key/value pair
   *
   * @param {*} key key
   * @param {*} value value
   * @param {string} type Type = 'S'tring, 'J'son, 'N'umeric
   */
  async set (key, value, type = 'S') {
    let val = value
    switch (type) {
      case 'S': val = '"' + val + '"' // String
        break
      case 'J': val = '\'' + JSON.stringify(value) + '\'' // JSON
        break
    }
    const sql = 'INSERT OR REPLACE INTO settings (key, value, type) VALUES ("' + key + '",' + val + ', "' + type + '")'
    return this.runSQL(sql)
  }

  /**
   * Get key/value pair
   *
   * @param {*} key to get
   * @returns {JSON} value
   */
  get (key) {
    return new Promise(async (resolve, reject) => {
      const sql = 'SELECT * FROM settings WHERE key="' + key + '"'
      const res = await this.getSQL(sql)
      if (res) {
        resolve((res.type === 'J') ? JSON.parse(res.value) : res.value)
      }
      else resolve('')
    })
  }

  /**
   * Insert a Contact
   *
   * @param {object} contact Contact details
   */
  async insertContact (contact) {
    const sql = 'INSERT OR REPLACE INTO contacts ' +
      ' (did, didMethod, matrixUser, matrixFederation, roomId, type, status ) VALUES (' +
      '\'' + contact.did + '\',' +
      '\'' + contact.didMethod + '\',' +
      '\'' + contact.matrixUser + '\',' +
      '\'' + contact.matrixFederation + '\',' +
      '\'' + contact.room_id + '\',' +
      '\'' + contact.type + '\',' +
      '\'created\')'
    return this.runSQL(sql)
  }

  /**
   * Contact Accepted
   *
   * @param {string} did DID
   * @param {string} membership Join or Leave
   */
  async updateContact (did, membership) {
    const update = (membership === 'join') ? 'joinAt=CURRENT_TIMESTAMP' : 'leaveat=CURRENT_TIMESTAMP'
    const sql = 'UPDATE contacts SET status=\'accepted\',' + update + '  WHERE did=\'' + did + '\''
    return this.runSQL(sql)
  }

  /**
   * Get contact from database by DID
   *
   * @param {string} did DID
   * @returns {object} Contact details
   */
  async getContact (did) {
    return new Promise(async (resolve) => {
      let contact = await this.getSQL('SELECT * FROM contacts WHERE did=\'' + did + '\'')
      if (contact)
      {
        const key = await this.getSQL('SELECT * FROM keys WHERE contactId=' + contact.id + ' AND status=\'active\'')
        contact.key = (key) ? key : false  
      }
      resolve(contact)
    })
  }

    /**
   * Get contact from database by DID
   *
   * @param {integer} contactId COntact ID
   * @returns {object} Contact details
   */
  async getContactById (contactId) {
    return new Promise(async (resolve) => {
      const res = await this.getSQL('SELECT * FROM contacts WHERE id=' + contactId)
      resolve(res)
    })
  }


  /**
   * Get Recipe from database by recipeId
   *
   * @param {number} recipeId Recipe Id
   * @returns {Promise} Recipe details
   */
  async getRecipe (recipeId) {
    return new Promise(async (resolve) => {
      const res = await this.getSQL('SELECT * FROM recipes WHERE recipeId=\'' + recipeId + '\'')
      resolve(res)
    })
  }

  /**
   * Get Steps from database by recipeId
   *
   * @param {number} recipeId Recipe Id
   * @returns {Promise} Recipe details
   */
  async getSteps (recipeId) {
    return new Promise(async (resolve) => {
      const res = await this.allSQL('SELECT * FROM steps WHERE recipeId=\'' + recipeId + '\' ORDER BY step')
      resolve(res)
    })
  }

  /**
   * Get the contact from a Room ID
   *
   * @param {string} roomId Romm ID
   * @returns {object} Contact details
   */
  async getContactByRoomId (roomId) {
    return new Promise(async (resolve, reject) => {
      const result = await this.getSQL('SELECT * FROM contacts WHERE roomId=\'' + roomId + '\'')
      resolve(result)
    })
  }

  /**
   * Adds a key.
   *
   * @param {object} contact Contact details
   */
  async addKeys (contactId, key) {
    let keyNumber = 1

    // Active Key
    let sql = 'SELECT * FROM keys where contactId=' +contactId+ ' AND status=\'active\''
    const lastKey = await this.getSQL(sql)
    if (lastKey) {
      keyNumber = lastKey.keyNumber+1
      sql = 'UPDATE keys set status=\'rotated\', rotatedAt=TIMESTAMP WHERE where contactId=' +contactId+ ' AND status=\'active\''
      await this.runSQL(sql)
    }
    
    sql = 'INSERT INTO keys (contactId, keyNumber, status, key) VALUES (' +contactId + ',' + keyNumber + ',' + '\'active\',\''+JSON.stringify(key)+'\')'
    return this.runSQL(sql)
  }

  /**
   * Get Contacts from
   *
   * @returns {Promise} Recipe details
   */
  async getContacts (type = false) {
    return new Promise(async (resolve) => {
      const where = (type !== false) ? ' WHERE type=\''+type+'\'' : ''
      console.log('SELECT * FROM contacts'+where)
      const res = await this.allSQL('SELECT * FROM contacts'+where)
      resolve(res)
    })
  }

  /**
   * Init Database Tables
   *
   * @returns {Promise} query results
   */
  async init () {
    return new Promise((resolve, reject) => {
      DBTables.reduce((accumulatorPromise, nextSQL) => {
        return accumulatorPromise.then(() => {
          return new Promise((resolve, reject) => {
            this.db.run(nextSQL, (err) => {
              /* istanbul ignore if */
              if (err) {
                reject(err)
                return
              }
              resolve()
            })
          })
        })
      }, Promise.resolve())
      resolve()
    })
  }

  /**
   * Close database
   */
  close () {
    this.db.close()
  }
}
