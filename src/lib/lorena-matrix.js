'use strict'
const axios = require('axios')

// Logger
const Logger = require('./logger')
const logger = new Logger()

const { dispatch } = require('nact')

/**
 * Javascript Class to interact with Zenroom.
 */
module.exports = class Matrix {
  constructor (homeserver = process.env.SERVER_MATRIX) {
    this.api = homeserver + '/_matrix/client/r0/'
    this.connection = {}
    this.txnId = 1
  }

  /**
   * Connects to a matrix server.
   *
   * @param {string} username Matrix username
   * @param {string} password Matrix password
   * @returns {Promise} Return a promise with the connection when it's done.
   */
  async connect (username, password) {
    return new Promise((resolve, reject) => {
      axios.get(this.api + 'login')
        .then(async () => {
          const result = await axios.post(this.api + 'login', {
            type: 'm.login.password',
            user: username,
            password: password
          })
          this.connection = result.data
          resolve(result.data.access_token)
        })
        .catch((_error) => {
          /* istanbul ignore next */
          reject('Could not connect to Matrix' + _error)
        })
    })
  }

  /**
   * Register user
   *
   * @param {string} username Matrix username
   * @param {string} password Matrix password
   * @returns {Promise} result
   */
  async register (username, password) {
    return new Promise((resolve, reject) => {
      axios.post(this.api + 'register', {
        auth: { type: 'm.login.dummy' },
        username: username,
        password: password
      })
        .then(async (res) => {
          resolve(username)
        })
        .catch((error) => {
          /* istanbul ignore next */
          reject(error)
        })
    })
  }

  /**
   * Listen to events.
   *
   * @param {Array} context Array of objects with the context: Database, Actors...
   * @param {string} nextBatch next batch of events to be asked to the matrix server.
   * @returns {Promise} Return a promise with the Name of the user.
   */
  async events (context, nextBatch) {
    return new Promise((resolve, reject) => {
      const apiCall = this.api +
        'sync?timeout=20000' +
        '&access_token=' + this.connection.access_token +
        (nextBatch === '' ? '' : '&since=' + nextBatch)

      // Sync with the server
      axios.get(apiCall).then(async (res) => {
        // Get Incoming invitations
        this.getIncomingInvitations(context, res.data.rooms.invite)

        // Get Accepted invitations
        this.getUpdatedInvitations(context, res.data.rooms.join)

        // Get Messages
        this.getMessages(context, res.data.rooms.join)

        resolve(res.data.next_batch)
      })
        .catch((error) => {
        /* istanbul ignore next */
          logger.error(error)
          /* istanbul ignore next */
          reject(error)
        })
    })
  }

  /**
   * Checks if the username is available
   *
   * @param {string} username to check
   * @returns {Promise} of true if username is available
   */
  async available (username) {
    return new Promise((resolve, reject) => {
      axios.get(this.api + 'register/available?username=' + username)
        .then(async (res) => {
          resolve(true)
        })
        .catch((_error) => {
          /* istanbul ignore next */
          resolve(false)
        })
    })
  }

  /**
   * Opens a connection to another user.
   *
   * @param {string} handlerTo User to connect with.
   * @param {string} handlerFrom User to connect from.
   * @param {string} type Type of the connection.
   * @returns {Promise} Return a promise with the Name of the user.
   */
  createConnection (did, didMethod, roomName, matrixUser, matrixFederation, type = 'contact') {
    let roomId = ''
    logger.key('Create connection to ', matrixUser)
    return new Promise((resolve, reject) => {
      const apiCreate = this.api + 'createRoom?access_token=' + this.connection.access_token
      axios.post(apiCreate, {name: roomName,visiblity: 'private' })
        .then((res, err) => {
          // Invite user to connect
          roomId = res.data.room_id
          const apiInvite = this.api + 'rooms/' + res.data.room_id + '/invite?access_token=' + this.connection.access_token
          return axios.post(apiInvite, { user_id: '@' + matrixUser + matrixFederation })
        })
        .then((res) => {
          const contact = {
            room_id: roomId,
            did: did,
            didMethod: didMethod,
            matrixUser: matrixUser,
            matrixFederation: matrixFederation,
            type: type,
          }
          resolve(contact)
        })
        .catch((error) => {
          console.log(error)
          reject('Could not create room')
        })
    })
  }

  /**
   * Sends a Message.
   *
   * @param {string} roomId Room to send the message to.
   * @param {string} type Type of message.
   * @param {string} body Body of the message.
   * @returns {Promise} Result of sending a message
   */
  sendMessage (roomId, type, body, token = false) {
    return new Promise((resolve, reject) => {
      const apiToken = (token === false) ? this.connection.access_token : token
      const apiSendMessage = this.api + 'rooms/' + roomId + '/send/m.room.message/' + this.txnId + '?access_token=' +  apiToken
      console.log("== MSG "+type)
      console.log(body)
      axios.put(apiSendMessage, {
        msgtype: type,
        body: body
      })
        .then((res, err) => {
          this.txnId++
          resolve(res)
        })
        .catch((error) => {
          console.log(error)
          reject('Could not create room')
        })
    })
  }

  /**
   * Accepts invitation to join a room.
   *
   * @param {string} roomId RoomID
   * @returns {Promise} Result of the SQL Query
   */
  acceptConnection (roomId) {
    return new Promise((resolve, reject) => {
      const apiAccept = this.api + 'rooms/' + roomId + '/join?access_token=' + this.connection.access_token
      axios.post(apiAccept, {})
        .then((res) => {
          resolve(res)
        })
        .catch((_error) => {
          reject('Could not accept invitation')
        })
    })
  }

  /**
   * Extract Invitations fro mthe API Call to matrix server - events
   *
   * @param {Array} context Array of objects with the context: Database, Actors...
   * @param {object} rooms Array of events related to rooms
   */
  getIncomingInvitations (context, rooms) {
    const matrix = this
    const roomEmpty = !Object.keys(rooms).length === 0 && rooms.constructor === Object
    if (!roomEmpty) {
      for (const roomId in rooms) {
        let invitation = {
          roomId: roomId
        }
        rooms[roomId].invite_state.events.forEach(element => {
          if (element.type === 'm.room.join_rules') {
            invitation = {
              ...invitation,
              sender: element.sender,
              join_rule: element.content.join_rule
            }
          } else if (element.type === 'm.room.member') {
            if (element.state_key === element.sender) {
              invitation = {
                ...invitation,
                membership: element.content.membership
              }
            } else {
              invitation = {
                ...invitation,
                origin_server_ts: element.origin_server_ts,
                event_id: element.event_id
              }
            }
          }
        })
        invitation.info = this.extratctDid(invitation.sender)
        const message = {
          roomId: roomId,
          info: this.extratctDid(invitation.sender),
          contact: {},
          content: {},
        }

        const addInvitationService = context.register.resolveAction("addInvitationService")
        dispatch(addInvitationService.doStep, { ...context, message, matrix })
      }
    }
  }

  /**
   * Extract Accepted Invitations fro mthe API Call to matrix server - events
   *
   * @param {Array} context Array of objects with the context: Database, Actors...
   * @param {object} rooms Array of events related to rooms
   * @returns {Array} Array of all the new invitations to connect.
   */
  async getUpdatedInvitations (context, rooms) {
    const matrix = this
    // const me = '@'+context.info.matrixUser+context.info.matrixFederation
    // Check if rooms is empty
    const roomEmpty = !Object.keys(rooms).length === 0 && rooms.constructor === Object
    if (!roomEmpty) {
      for (const roomId in rooms) {
        // Get the events in the Timeline.
        const events = rooms[roomId].timeline.events
        if (events.length > 0) {
          for (let i = 0; i < events.length; i++) {
            const element = events[i]
            // Get events for type m.room.member with membership join or leave.
            if ( element.type === 'm.room.member')
            {
                const contact = await context.database.getContactByRoomId(roomId)
                const matrixUser = (contact) ? '@'+contact.matrixUser+contact.matrixFederation : false
                if ( element.content.membership === 'join') {
                // Don't update our own events and only allow previusly created channels.
                if (element.sender === matrixUser) {
                  const invitation = {
                    roomId: roomId,
                    sender: element.sender,
                    membership: element.content.membership,
                    origin_server_ts: element.origin_server_ts,
                    event_id: element.event_id,
                    contact: contact
                  }
                  const updateInvitationService = context.register.resolveAction("updateInvitationService")
                  dispatch(updateInvitationService.doStep, { ...context, invitation, matrix })
                }
                else if  (element.content.membership === 'leave') {
                  //TODO: Update connections status. The other peer left.
                }
              }
            }
          }
        }
      }
    }
  }

  /**
   * Extract Messages from events
   *
   * @param {Array} context Array of objects with the context: Database, Actors...
   * @param {object} rooms Array of events related to rooms
   * @returns {Array} Array of all the new invitations to connect.
   */
  getMessages (context, rooms) {
    const matrix = this
    return new Promise(async (resolve, reject) => {
      const messages = []

      // Check if rooms is empty
      const roomEmpty = !Object.keys(rooms).length === 0 && rooms.constructor === Object
      if (!roomEmpty) {
        for (const roomId in rooms) {
          // Get the events in the Timeline.
          const events = rooms[roomId].timeline.events
          if (events.length > 0) {
            for (let i = 0; i < events.length; i++) {
              const element = events[i]
              
              // Get events for type m.room.member with membership join or leave.
              if (element.type === 'm.room.message') {
                const contact = await context.database.getContactByRoomId(roomId)
                
                // Error: Contact is not in the Database.
                if (!contact) {
                  resolve(false)
                }
                // Don't update our own events.
                
                const matrixUser = '@'+contact.matrixUser+contact.matrixFederation
                
                if (element.sender === matrixUser) {
                  const message = {
                    info: context.info,
                    contact: contact,
                    content: element.content,
                    origin_server_ts: element.origin_server_ts,
                  }
                  // Dispatch action.
                  if (message.content.msgtype === 'm.action' && contact.status === 'accepted') {
                    // Contact exists in our DB and msgtype is 'action'
                    const actionService = context.register.resolveAction("actionService")
                    dispatch(actionService.doStep, { ...context, message, matrix })
                  } else if (message.content.msgtype === 'm.text' && contact.type === 'client' && contact.status === 'accepted') {
                    // We are listening to one of our clients.
                    const messageService = context.register.resolveAction("messageService")
                    dispatch(messageService.doStep, { ...context, message, matrix })
                  }
                }
              }
            }
          }
        }
      }
      resolve(messages)
    })
  }

  /**
   * Returs the did associated to a matrix user
   *
   * @param {string} sender Sender
   * @returns {string} Full DID
   */
  extratctDid (sender) {
    const parts = sender.split(':')
    return {
      matrixUser: parts[0].substr(1),
      matrixFederation: parts[1]
    }
  }

  callBack () {
    // Check did in process_id + additional security checks

    console.log('Callback')
  }
}
