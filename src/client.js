// Init your Fruit Vault (Identity Container)

const Index = require('./lib/lorena-index')

const main = async (didURL, password) => {
  let did = didURL.split(":")
  let id = did.pop()
  new Index(did.join(":")+':')
    .addClient(id, password)
    .then(() => {
      console.log('Finished')
    })
}
main(process.argv[2], process.argv[3])
