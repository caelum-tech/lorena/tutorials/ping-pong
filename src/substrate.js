// wss://substrate-testnet.caelumlabs.com
// const Index = require('./lib/lorena-index')
const { ApiPromise, WsProvider, Keyring } = require('@polkadot/api')
const { H256, Raw } = require('@polkadot/types')
// const { Bytes, H256, Hash, Raw } = require('@polkadot/types')
// const { ApiPromise, WsProvider, Keyring } = require('@polkadot/api')
const UNITS = 1000000000000
const TO = '5DRf573YJE52TvA2Mv76772BxzxwrcsYuc1rFr7j37iroeL6'

const transfer = async (api, keypair) => {
  const balance = await api.query.balances.freeBalance(keypair.address)
  const nonce = await api.query.system.accountNonce(keypair.address)
  console.log('Address : ' + keypair.address)
  console.log('Balance : ' + balance)
  console.log('Send    : ' + 10000 * UNITS)
  console.log('Nonce : ' + nonce)

  // Sign and send the transaction using our account
  const unsub = await api.tx.balances
    .transfer(TO, 100 * UNITS)
    .signAndSend(keypair, (result) => {
      console.log(`Current status is ${result.status}`)

      if (result.status.isFinalized) {
        console.log(`Transaction included at blockHash ${result.status.asFinalized}`)
        unsub()
      }
    })
}
const main = async () => {
  const provider = new WsProvider('wss://substrate-demo.caelumlabs.com/')
  const api = await ApiPromise.create({ provider })
  const keyring = new Keyring({ type: 'sr25519' })
  const keypair = keyring.addFromUri('//Alice')

  transfer(api, keypair)
  // TODO: create a DID and test everything
  // const bytes = new Bytes('Hello')
  // console.log(bytes)

  const h1 = new H256(32, 'Caelum')
  const h2 = new H256(32, 'Alex')

  const raw = new Raw(32, 'Alex')
  console.log(h1)
  console.log(h2)
  console.log(raw.hash())
  // console.log(H256.from())
}
main().catch(console.error)
