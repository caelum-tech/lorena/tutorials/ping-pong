
const processAction = (system) => {
  return {
    args: ['did', 'type'],
    steps: [
      {
        type: 'start',
        recipe: 'credential_issue',
        to: 'did'
      },
      {
        type: 'receive_credential'
      },
      {
        type: 'send',
        to: 'cli'
      }
    ],
    doStep: async (recipe, ctx) => {
      let msg
      switch (recipe.recipeInfo.currentStep) {
        case 1:
          msg = {
            action: 'issue',
            type: recipe.arguments.type
          }
          await recipe.next(msg)
          break
        case 2:
          await recipe.next()
          break
        case 3:
          // Get payload and send Hello
          msg = await recipe.getPayload(2)
          await recipe.next('New credential received - keyIndex=' + msg.credentialSubject.keyIndex + ' - credentialId=' + msg.credentialSubject.credentialId)
          break
      }
    }
  }
}

module.exports.processAction = processAction
