
const processAction = (system) => {
  return {
    args: ['did'],
    steps: [
      {
        type: 'start',
        recipe: 'transfer',
        to: 'did'
      },
      {
        type: 'receive'
      },
      {
        type: 'send',
        to: 'cli'
      }
    ],
    doStep: async (recipe, ctx) => {
      console.log('FAUCET step :' + recipe.recipeInfo.currentStep)
      switch (recipe.recipeInfo.currentStep) {
        case 1:
          await recipe.next({ addr: recipe.address })
          break
        case 2:
          // Receive instructions & save them into payload.
          console.log('receive GAS & save payload')
          await recipe.next()
          break
        case 3:
          // Get payload and send Hello
          await recipe.next('received 10 tokens')
          break
      }
    }
  }
}

module.exports.processAction = processAction
