
const processAction = (system) => {
  return {
    steps: [
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, ctx) => {
      await recipe.next('DID:' + recipe.info.didMethod+recipe.info.did+
        '\nMatrix: @'+recipe.info.matrixUser+recipe.info.matrixFederation)
    }
  }
}

module.exports.processAction = processAction
