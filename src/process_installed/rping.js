
const processAction = (system) => {
  return {
    args: ['did'],
    steps: [
      {
        type: 'start',
        recipe: 'ping',
        contact: 'did'
      },
      {
        type: 'receive',
        contact: 'did'
      },
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, ctx) => {
      let msg
      switch (recipe.recipeInfo.currentStep) {
        case 1:
          await recipe.next()
          break
        case 2:
          await recipe.next()
          break
        case 3:
          // Get payload and send Hello
          msg = await recipe.getPayload(2)
          msg = recipe.contact.did+' says ' + msg
          await recipe.next(msg)
          break
      }
    }
  }
}

module.exports.processAction = processAction
