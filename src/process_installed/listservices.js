
const processAction = (system) => {
  return {
    name: "listservices",
    version: "0.1",
    description: "List services registered",
    helpText: "Returns a list of all services registerd",
    steps: [
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, _ctx) => {
      const actors = recipe.register.getRegisteredActors()
      const data = []
      actors.forEach((actor) => {
        data.push(
        {
          name: actor.name,
          version: actor.version,
          description: actor.description,
          active: actor.active,
        }
        )
      }) 
      await recipe.next(data)
      //await recipe.next(JSON.stringify(data))
        // 'balance\nYour balance in tokens\n\n' +
        // 'transfer addr <address> total <tokens>\nYour own DID\n\n')
    }
  }
}

module.exports.processAction = processAction
