
const processAction = (system) => {
  return {
    args: ['did'],
    steps: [
      {
        type: 'send'
      },
      {
        type: 'accepted'
      },
      {
        type: 'receive'
      }

    ],
    doStep: async (recipe, ctx) => {
      //if (recipe.arguments.did && recipe.arguments.did.connected === false) {
        // Connect.
        //TODO: Get MatrixUser from IPFS & Blockchain
        recipe.matrix.createConnection (recipe.arguments.did.did, 'did:lor:lab:', '', recipe.arguments.did.did, ':matrix.caelumlabs.com', 'contact')
        .then(async (contact) => {
            await recipe.database.insertContact(contact)
            await recipe.next('You sent an invitation to connect to ' + recipe.arguments.did.did)
          })
      /*} else {
        // Not connected.
        await recipe.next('You are already connected to ' + recipe.arguments.did.did)
      }*/
    }
  }
}

module.exports.processAction = processAction
