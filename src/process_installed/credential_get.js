
const processAction = (system) => {
  return {
    args: ['did', 'type'],
    steps: [
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, ctx) => {
      console.log(recipe.arguments)

      const credential = await recipe.getCredential(recipe.arguments.type, recipe.arguments.did.did)
      console.log(credential)
      await recipe.next('Credential exists - keyIndex=' + credential.keyId + ' - credentialId=' + credential.credentialIndex)
    }
  }
}

module.exports.processAction = processAction
