
const processAction = (system) => {
  return {
    name: "ping",
    version: "0.2",
    description: "Enhaced test service to test system response",
    steps: [
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, ctx) => {
      await recipe.next('Pong')
    }
  }
}

module.exports.processAction = processAction
