
const processAction = (system) => {
  return {
    steps: [
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, _ctx) => {
      await recipe.next(
        'Available commands\n\n' +
        'ping\nPing your IDSpace\n\n' +
        'whoami\nYour own DID\n\n' +
        'hello\nHello World example\n\n' +
        'info did <did>\nDIDs info\n\n' +
        'rping did <did>\nPing another IDSpace\n\n')
        // 'balance\nYour balance in tokens\n\n' +
        // 'transfer addr <address> total <tokens>\nYour own DID\n\n')
    }
  }
}

module.exports.processAction = processAction
