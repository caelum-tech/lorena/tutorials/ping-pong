
const processAction = (system) => {
  return {
    args: ['addr'],
    steps: [
      {
        type: 'smart'
      }
    ],
    doStep: async (recipe, ctx) => {
      console.log('=== Transfer 10 tokens')

      await recipe.blockchain.transfer(recipe.arguments.addr, 10)
      await recipe.next(JSON.stringify({}))
    }
  }
}

module.exports.processAction = processAction
