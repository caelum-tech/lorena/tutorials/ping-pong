
const processAction = (system) => {
  return {
    args: ['did', 'productId', 'bank'],
    steps: [
      {
        type: 'start',
        recipe: 'product_payment',
        contact: 'did'
      },
      {
        type: 'receive'
      },
      {
        type: 'send',
        contact: 'cli'
      },
      {
        type: 'receive'
      },
      {
        type: 'start',
        recipe: 'payment',
        contact: 'bank'
      },
      {
        type: 'receive'
      },
      {
        type: 'send',
        contact: 'cli'
      }
    ],
    doStep: async (recipe, ctx) => {
      let msg, payload, options, price
      switch (recipe.recipeInfo.currentStep) {
        case 1:
          msg = {
            action: 'buy',
            productId: recipe.arguments.productId
          }
          await recipe.next(msg)
          break
        case 2:
          await recipe.next()
          break
        case 3:
          // Get payload and send Hello
          msg = await recipe.getPayload(2)
          payload = ''
          if (msg.type === 'priceOptions') {
            payload += '\n- Option 1 - Buy it for ' + msg.credentialSubject[0].price + ' €'
            // Get credential
            const cred = await recipe.getCredential(msg.credentialSubject[1].credentialType, msg.credentialSubject[1].issuer)
            if (cred) {
              payload += '\n\n- Option 2 - Buy it for ' + msg.credentialSubject[1].price + ' €'
              payload += '\n  * You will be sharing your credential ' + msg.credentialSubject[1].credentialType + ' with the ecommerce '
            }
            payload += '\n\n*** Answer:\nproduct_buy recipe ' + recipe.recipeInfo.recipeId + ' option 1/2'
          }
          await recipe.next(payload)
          break
        case 4:
          await recipe.next()
          break
        case 5:
          msg = await recipe.getPayload(4)
          options = await recipe.getPayload(2)
          price = (msg[1] === '1') ? options.credentialSubject[0].price : options.credentialSubject[1].price
          payload = {
            payment: {
              price: price,
              iva: Math.round(((price * 0.21) + Number.EPSILON) * 100) / 100,
              total: Math.round(((price * 1.21) + Number.EPSILON) * 100) / 100,
              iban: "ES6000491500051234567892"
            }
          }
          await recipe.next(payload)
          break
        case 6:
          await recipe.next()
          break
        case 7:
          msg = await recipe.getPayload(4)
          await recipe.next('Payment processed')
          break
      }
    }
  }
}

module.exports.processAction = processAction
