
const processAction = (system) => {
  return {
    name: "ping",
    version: "0.1",
    description: "Test service to test system response",
    steps: [
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, ctx) => {
      await recipe.next('pong')
    }
  }
}

module.exports.processAction = processAction
