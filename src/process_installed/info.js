/**
 * Definition of "info" recipe
 * When the argument is "did" it looks into the DB to see if you are connected or not
 *
 * @param {object} system System object for NACT
 * @returns {object} Arguments and steps.
 */
const processAction = (system) => {
  return {
    args: ['did'],
    steps: [
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, ctx) => {
      if (recipe.arguments.did && recipe.arguments.did.connected === false) {
        // Connected.
        await recipe.next('You are NOT connected to ' + recipe.arguments.did.did)
      } else {
        // Not connected.
        await recipe.next('You are connected to ' + recipe.arguments.did.did)
      }
    }
  }
}

module.exports.processAction = processAction
