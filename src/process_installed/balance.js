
const processAction = (system) => {
  return {
    steps: [
      {
        type: 'send',
        format: 'text'
      }
    ],
    doStep: async (recipe, ctx) => {
      const balance = 'Your balance ' + recipe.blockchain.keypair.address + ' is ' + await recipe.blockchain.balance()
      await recipe.next(balance)
    }
  }
}

module.exports.processAction = processAction
