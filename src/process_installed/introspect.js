
const processAction = (system) => {
  return {
    name: "introspect",
    version: "0.1",
    description: "Introspect a recipe",
    helpText: "",
    args: ['actor'],
    steps: [
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, _ctx) => {
      const arg = recipe.arguments.actor
      // check if recipe is versioned
      let name = arg
      let version = recipe.arguments.version
      if (!version && name.indexOf(":") !== -1) {
        [name, version] = arg.split(":", 2)
      }

      const actor = recipe.register.resolve(name, version)
      const data =  {
          name: actor.name,
          version: actor.version,
          description: actor.description,
          active: actor.active,
          helpText: actor.action.helpText,
          steps: actor.action.steps, 
          args: actor.action.args,
        }
      await recipe.next(data)
    }
  }
}

module.exports.processAction = processAction
