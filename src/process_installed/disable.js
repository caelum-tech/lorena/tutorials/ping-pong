
const processAction = (system) => {
  return {
    name: "disable",
    version: "0.1",
    description: "Disable a recipe",
    helpText: "",
    args: ['actor'],
    steps: [
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, _ctx) => {
      const arg = recipe.arguments.actor
      // check if recipe is versioned
      let name = arg
      let version = recipe.arguments.version
      if (!version && name.indexOf(":") !== -1) {
        [name, version] = arg.split(":", 2)
      }

      const actor = await recipe.register.disable(name, version)
      const data =  {
          name: actor.name,
          version: actor.version,
          description: actor.description,
          active: actor.active,
        }
      await recipe.next(data)
    }
  }
}

module.exports.processAction = processAction
