
const processAction = (system) => {
  return {
    args: ['action', 'type'],
    steps: [
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, ctx) => {
      console.log('READY TO CRED')
      const subject = {
        id: recipe.recipeInfo.createdBy,
        issuer: recipe.did,
        issuanceDate: Date().toString(),
        keyIndex: 1,
        credentialId: 32,
        hasOccupation: {
          '@type': 'Occupation',
          name: 'Developer'
        }
      }
      const cred = {
        '@context': 'https://www.w3.org/2018/credentials/v1',
        id: 'did:lor:lab:caelum?key=1&credential=32',
        type: ['hasOccupation', 'VerifiableCredential'],
        credentialSubject: subject,
        proof: { }
      }
      await recipe.next(cred)
    }
  }
}

module.exports.processAction = processAction
