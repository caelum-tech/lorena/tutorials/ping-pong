
const processAction = (system) => {
  return {
    args: ['payment'],
    steps: [
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, ctx) => {
      const pay = recipe.arguments.payment
      console.log(recipe.recipeInfo)
      console.log('********************************************')
      console.log('INCOMING PAYMENT')
      console.log('Price         : ' + pay.price + '€')
      console.log('IVA           : ' + pay.iva + '€')
      console.log('Total         : ' + pay.total + '€')
      console.log('Transfer to   :' + pay.iban)
      console.log('********************************************')
      console.log('\n\n')
      await recipe.next({result : 'Accepted'})
    }
  }
}

module.exports.processAction = processAction
