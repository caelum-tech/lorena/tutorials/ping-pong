
const processAction = (system) => {
  return {
    steps: [
      {
        type: 'send'
      },
      {
        type: 'receive'
      },
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, _ctx) => {
      let payload = ''
      switch (recipe.recipeInfo.currentStep) {
        case 1:
          // Send instructions.
          payload = 'Your name is?\n The answer must be: >hello recipe ' + recipe.recipeInfo.recipeId + ' Your_Name'
          await recipe.next(payload)
          break
        case 2:
          // Receive instructions & save them into payload.
          await recipe.next()
          break
        case 3:
          // Get payload and send Hello
          payload = 'Hello ' + await recipe.getPayload(2)
          await recipe.next(payload)
          break
      }
    }
  }
}

module.exports.processAction = processAction
