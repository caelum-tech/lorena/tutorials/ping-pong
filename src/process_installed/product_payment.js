
const processAction = (system) => {
  return {
    args: ['action', 'productId'],
    steps: [
      {
        type: 'send'
      },
      {
        type: 'receive'
      },
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, ctx) => {
      let subject, cred, msg
      switch (recipe.recipeInfo.currentStep) {
        case 1:
          subject = [{
            option: 1,
            credentialType: 'none',
            price: 10
          }, {
            option: 2,
            credentialType: 'hasOccupation',
            issuer: 'didissuer',
            price: 7
          }]
          cred = {
            '@context': 'https://www.w3.org/2018/credentials/v1',
            id: 'did:lor:lab:caelum?product_id=' + recipe.arguments.productId,
            type: 'priceOptions',
            credentialSubject: subject
          }
          await recipe.next(JSON.stringify(cred))
          break
        case 2:
          await recipe.next()
          break
        case 3:
          msg = await recipe.getPayload(2)
          await recipe.next(cred)
          break
      }
    }
  }
}

module.exports.processAction = processAction
