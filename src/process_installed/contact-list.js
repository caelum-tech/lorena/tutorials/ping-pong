
const processAction = (system) => {
  return {
    steps: [
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, _ctx) => {
    
        let contacts = await recipe.database.getContacts()
        await recipe.next(contacts)
    },
  }
}

module.exports.processAction = processAction
