
const processAction = (system) => {
  return {
    name: "clients-list",
    version: "0.1",
    description: "List active clients",
    steps: [
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, ctx) => {
      let contacts = await recipe.database.getContacts('client')
      console.log(contacts)
      await recipe.next(contacts)
    }
  }
}

module.exports.processAction = processAction
