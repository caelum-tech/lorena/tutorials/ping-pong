
const processAction = (system) => {
  return {
    name: "revoke",
    version: "0.1",
    description: "Revoke acces to a recipe to a client",
    helpText: "",
    args: ['did', 'actor'],
    steps: [
      {
        type: 'send'
      }
    ],
    doStep: async (recipe, _ctx) => {
      const arg = recipe.arguments.actor
      // check if recipe is versioned
      let name = arg
      let version = recipe.arguments.version
      if (!version && name.indexOf(":") !== -1) {
        [name, version] = arg.split(":", 2)
      }

      const actor = await recipe.register.resolve(name, version)
      
      let contact = recipe.arguments.did
      if (!contact.id) {
        contact = await recipe.database.getContactById(recipe.arguments.did.did)
      }
      await recipe.auth.revoke(contact.id, actor.name, actor.version)      
      const data =  {
          status: "ok",
          text: "revoked",
        }
      await recipe.next(data)
    }
  }
}

module.exports.processAction = processAction
