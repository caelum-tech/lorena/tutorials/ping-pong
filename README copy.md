# Lorena Personal Data Store (PDS)

The Personal Data Store holds the private keys, verifiable credentials, and other sensitive data for a [W3C](https://www.w3.org/) [DID](https://www.w3.org/TR/did-core/).  Lorena PDS is part of the [Lorena Hub](https://gitlab.com/caelum-tech/lorena/lorena-sdk).

|Branch|Pipeline|Coverage|
|:-:|:-:|:-:|
|[`master`](https://gitlab.com/caelum-tech/lorena/lorena-pds/tree/master)|[![pipeline status](https://gitlab.com/caelum-tech/lorena/lorena-pds/badges/master/pipeline.svg)](https://gitlab.com/caelum-tech/lorena/lorena-pds/commits/master)|[![coverage report](https://gitlab.com/caelum-tech/lorena/lorena-pds/badges/master/coverage.svg)](https://caelum-tech.gitlab.io/lorena/lorena-pds)|


## Install
```bash
npm i
```

## Run tests
Run the tests
```bash
npm test
```

## ENV Variables
Needed enviroment Vars to execute.
```bash
export SERVER_MATRIX=https://matrix.caelumlabs.com
export SERVER_SUBSTRATE=wss://substrate-demo.caelumlabs.com/
export SERVER_IPFS=https://ipfs ... //TODO
```

## Init your IDSpace
First time DID initialization
```bash
./idspace init did:lor:lab: "Your Name or Org" -p password
```

## Init your IDSpace
First time DID initialization
```bash
./idspace addclient --handler=db74c940d447e877
```

It will return something like :
Client CODE d190926ca9508f2c-6d87eb48c6224700-db74c940d447e877
This is your client code to use for a client

## Open your IDSpace
First time DID initialization
```bash
export PDS_password=password
./idspace run

./idspace run --password=nikola
```


## See also
[Documentation](./docs/pds.md)
