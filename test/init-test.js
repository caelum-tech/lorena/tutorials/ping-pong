const chai = require('chai')
const uuidv4 = require('uuid/v4')
const DID = require('../src/lib/lorena-did')

// Configure chai
chai.should()
const uuid = uuidv4()

describe('lorena-init unit tests', function () {
  let did

  it('should create new Initializer', () => {
    did = new DID(uuid)
    did.should.not.be.undefined
  })

  it('should run Initializer', async () => {
    const result = await did.new('password')
    result.should.be.eq(did)
  })
})
