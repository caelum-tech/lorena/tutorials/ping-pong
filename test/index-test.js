const chai = require('chai')
const uuidv4 = require('uuid/v4')
const Index = require('../src/lib/lorena-index')

// Configure chai
chai.should()

describe('lorena-index unit tests', function () {
  let index

  it('should initialize and run', async () => {
    const uuid = uuidv4()
    const password = uuidv4()
    index = new Index(uuid, password)
    await index.new()
    await index.run(5)
  })

  it('should stop index', async () => {
    await index.shutdown()
  })
})
