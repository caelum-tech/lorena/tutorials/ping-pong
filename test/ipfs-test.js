const chai = require('chai')
const IPFS = require('../src/lib/lorena-ipfs')
const uuidv4 = require('uuid/v4')
const diddoc = require('@caelum-tech/caelum-diddoc-nodejs')
const vcdm = require('@caelum-tech/caelum-vcdm-nodejs')

// Configure chai
chai.should()
const expect = chai.expect
const did = 'did:lor:lab:' + uuidv4()

describe('Interplanetary File System', function () {
  let ipfs, file

  it('should create a new IPFS instance', () => {
    ipfs = new IPFS()
  })

  it('should detect the IPFS daemon running', async () => {
    expect(await ipfs.connected()).to.be.true
  })

  it('should put a new file', async () => {
    const didDocument = new diddoc.DidDoc('https://www.w3.org/ns/did/v1', did)
    let vc
    try {
      vc = new vcdm.VClaim()
    } catch(err) {
        console.error(`Unexpected error in loadWasm. [Message: ${err.message}]`)
        return undefined
    }
    console.log(vc.toJSON())

    file = await ipfs.add(didDocument.toJSON())
  })

  it('should get that file', async () => {
    const result = await ipfs.get(file[0].path)
    result.id.should.eq(did)
  })
})
