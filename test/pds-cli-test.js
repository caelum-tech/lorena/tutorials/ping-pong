// const chai = require('chai')
const nixt = require('nixt')
const uuidv4 = require('uuid/v4')

// Configure chai
// chai.should()
// const expect = chai.expect

const uuid = uuidv4()
const did = `did:lor:lab:${uuid}`

describe('Personal Data Store Command-Line Interface', function () {
  it('should give help', (done) => {
    nixt()
      .run('./pds')
      .stdout(/Usage:/)
      .code(1)
      .end(done)
  })

  it('should initialize PDS', (done) => {
    nixt()
      .run(`./pds init ${uuid}`)
      .on(/Password:/gu)
      .respond('password\n')
      .stdout(/DIDDoc created/)
      .stdout(new RegExp(`Database Open ${did}`, 'g'))
      .end(done)
  })

  it('should run PDS process', (done) => {
    nixt()
      .run(`./pds ${uuid} 1`)
      .on(/Password:/gu)
      .respond('password\n')
      .stdout(new RegExp(`Database Open ${did}`, 'g'))
      .end(done)
  })
})
