const chai = require('chai')
const Index = require('../src/lib/lorena-index')
const Matrix = require('../src/lib/lorena-matrix')

// Configure chai
chai.should()

describe('lorena-recipes unit tests', function () {
  let index

  it('should initialize and run a client', async () => {
    index = new Index('did:lor:lab:')
    await index.run('9a2ff2824c98f450', 'nikola')
    console.log("Let's go!")
  })

  it( 'Shosuld connect as a client', async() => {
    const roomId = '!CgGydyLNOFafSFhRsI:matrix.caelumlabs.com'
    let matrix = new Matrix()
    let token = await matrix.connect('912c964e64264b3a', '5e5c150e2ea568da')
    await matrix.sendMessage(roomId, 'm.text','ping', token)

    //TODO: Do a proper testing of all recipes
  })
})
