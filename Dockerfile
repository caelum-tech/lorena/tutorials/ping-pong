# Caelum LABS Official Image <albert@caelumlabs.com>
# This image contains nodejs and caelum-worker API

# specify the node base image with your desired version node:<version>
# https://nodejs.org/es/about/releases/
FROM node:lts-buster
LABEL maintainer="Albert Valverde <albert@caelumlabs.com>"
LABEL description="Lorena idSpace Docker Image"

EXPOSE 3000

# Installing tools
RUN apt-get update
RUN apt-get install -y git bash curl grep python vim nmon net-tools build-essential apt-transport-https sqlite

# Cloning App repository
COPY . /home/node/app
WORKDIR /home/node/app

RUN  npm i

ENTRYPOINT ["./idspace"]